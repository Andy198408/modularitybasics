package andy.staticWorld;


public class StaticWorld {
//    will not be available here => package is only exported to helloOtherWorldModule
//    HelloTransitiveWorld htw = new HelloTransitiveWorld();

    public String giveStaticString(){
        return "String from StaticWorld";
    }

}
