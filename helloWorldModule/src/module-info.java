module helloWorldModule {
    exports andy.helloWorld;
    exports andy.staticWorld;

    requires transitiveModule;

    provides andy.helloWorld.HelloInterface with andy.helloWorld.HelloWorld, andy.helloWorld.HelloSecondWorld;
}